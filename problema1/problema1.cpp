#include <iostream>
#include "Inicio.h"
using namespace std;

int main(){

  /*Variables*/
  string line;
  int num = 1;

  Inicio inicio = Inicio();
  Lista *lista = inicio.get_Lista();

  cout << "Ingrese 0 para dejar de agregar numero a la lista" << endl;

  while(num != 0){
    cout << "Ingrese un numero entero: ";
    getline(cin, line);
    num = stoi(line);

    if(num != 0){
      /*Se crea nodo con el numero que ingresa*/
      lista->crearNodo(num);
      /*Se impime la lista*/
      cout << "|ESTADO ACTUAL|" << endl;
      /*Se ordena la lista*/
      lista->ordenar();
      lista->imprimir();
    }
  }

  /*Se imprime la lista*/
  cout << "\n\n|ESTADO FINAL|" << endl;
  lista->imprimir();

  return 0;
}
