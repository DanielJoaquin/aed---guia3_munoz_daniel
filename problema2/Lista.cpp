#include <iostream>
#include "Lista.h"
using namespace std;
/*Constructor*/
Lista::Lista(){}
/*Metodo crear Nodo*/
void Lista::crearNodo(int x){
  Nodo *temp;
  /*Se crea el nodo*/
  temp = new Nodo;
  /*Se le asigna un valor al nodo*/
  temp->numero = x;
  /*Se apuntara a NULL por defecto*/
  temp->sig = NULL;
  /*Si es el primer nodo en la lista, pasa a ser el ultimo tambien*/
  if(this->primero == NULL){
    this->primero = temp;
    this->ultimo = this->primero;
  }
  /*Si no*/
  else{
    /*El nodo apuntara a ultimo*/
    this->ultimo->sig = temp;
    /*Se deja como ultimo nodo al creado*/
    this->ultimo = temp;
  }
}
/*Metodo para ordenar lista de menor a mayor*/
void Lista::ordenar(){
  /*Nodos auxiliares*/
  Nodo *nodo = this->primero;
  Nodo *sucesor = NULL;
  int aux = 0;
  /*Se recorre la lista mientras el sucesor del primero sea NUlo*/
  while((nodo->sig) != NULL){
    sucesor = nodo->sig;
    /*SE recorre la lista mientra el nodo sucesor no sea NULO*/
    while(sucesor != NULL){
      /*Si el numero del primer nodo es mayor que el del sucesor se cambia*/
      if(nodo->numero > sucesor->numero){
        /*Se guarda el numero del nodo sucesor en variable auxiliar*/
        aux = sucesor->numero;
        /*El nodo sucesor toma el valor del numero del primer nodo*/
        sucesor->numero = nodo->numero;
        /*El primero nodo toma el valor de la variable auxiliar*/
        nodo->numero = aux;
      }
      /*Se avanza de nodo para seguir comparando*/
      sucesor = sucesor->sig;
    }
    /*SE avanza de nodo para seguor comparando*/
    nodo = nodo->sig;
  }
}
/*Metodo para imprimir*/
void Lista::imprimir(){
  /*temp toma el valor del primer nodo*/
  Nodo *temp = this->primero;

  cout << "--------------------------------------" << endl;
  /*Se recorre la lista hasta que el nodo actual apunte a NULo*/
  while(temp != NULL){
    cout << "( " << temp->numero << " )" << "-";
    /*temp tomara el valor del nodo al que apunta el nodo actual*/
    temp = temp->sig;
  }
  cout << "\n--------------------------------------" << endl;
}

/*Metodo para unir listas*/
Lista* Lista::fusionListas(Lista *lista1, Lista *lista2){
  /*Ultimo nodo de lista1 va apuntar al primer nodo de lista2*/
  lista1->ultimo->sig = lista2->primero;
  return lista1;;
}
