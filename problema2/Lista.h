#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

typedef struct Nodo{
  int numero=0;
  struct Nodo *sig;
} Nodo;

class Lista{
  private:
    Nodo *primero = NULL;
    Nodo *ultimo = NULL;
  public:
    /*Constructor*/
    Lista();
    /*Metodos*/
    void crearNodo(int x);
    void ordenar();
    void imprimir();
    Lista *fusionListas(Lista *lista1, Lista *lista2);

};
#endif
