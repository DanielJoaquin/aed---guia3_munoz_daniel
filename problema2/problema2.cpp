#include <iostream>
#include "Inicio.h"
using namespace std;

int main(){

  /*Variables*/
  string line;
  int num = 1;

  Inicio inicio = Inicio();
  Inicio inicio2 = Inicio();
  Inicio inicio3 = Inicio();

  Lista *lista1 = inicio.get_Lista();
  Lista *lista2 = inicio2.get_Lista();
  Lista *lista3 = inicio3.get_Lista();

  cout << "Ingrese 0 para dejar de agregar numero a la lista1" << endl;
  while(num != 0){
    cout << "Ingrese un numero entero: ";
    getline(cin, line);
    num = stoi(line);

    if(num != 0){
      /*Se crea nodo con el numero que ingresa*/
      lista1->crearNodo(num);
    }
  }

  num = 1;

  cout << "\nIngrese 0 para dejar de agregar numero a la lista2" << endl;
  while(num != 0){
    cout << "Ingrese un numero entero: ";
    getline(cin, line);
    num = stoi(line);

    if(num != 0){
      /*Se crea nodo con el numero que ingresa*/
      lista2->crearNodo(num);
    }
  }

  /*Se ordenan las listas*/
  lista1->ordenar();
  lista2->ordenar();
  /*Se imprimen la listas por separado*/
  cout << "\n\n|Lista1|" << endl;
  lista1->imprimir();
  cout << "\n\n|Lista2|" << endl;
  lista2->imprimir();

  /*SE llama al metodo para unir las lista*/
  lista3 = lista3->fusionListas(lista1, lista2);
  /*Se ordena la lista 3*/
  lista3->ordenar();
  /*Se imprime la lista3*/
  cout << "\n\n|Lista3|" << endl;
  lista3->imprimir();
  return 0;
}
